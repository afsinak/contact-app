var obj=angular.module("myProject",["ngRoute"]);
obj.config(function($routeProvider){
	$routeProvider
	  .when("/",{
		templateUrl : "home.html"
	})
	.when("/about",{
		templateUrl : "about.html"
	})
	.when("/signup",{
		templateUrl : "signup.html"
	})
	.when("/contact",{
		templateUrl : "contact.html"
	})
	.when("/login",{
		templateUrl : "login.html"
	})
});

obj.controller("pageValidation",function($scope){
	$scope.pagetest=function()
	{
		if(($scope.fname==undefined)||($scope.fname==""))
		{
			$scope.nameerror="pls enter name";
		}
		else
		{
		   $scope.nameerror="";	
		}
		if(($scope.email==undefined)||($scope.email==""))
		{
			$scope.emailerror="pls enter email";
		}
		else
		{
		   $scope.emailrror="";	
		}	
		if(($scope.pass==undefined)||($scope.pass==""))
		{
			$scope.passerror="pls enter password";
		}
		else
		{
		   $scope.passerror="";	
		}
		if(($scope.city==undefined)||($scope.city==""))
		{
			$scope.cityerror="pls enter city";
		}
		else
		{
		   $scope.cityerror="";	
		}
		if(($scope.mobile==undefined)||($scope.mobile==""))
		{
			$scope.mobileerror="pls enter number";
		}
		else
		{
		   $scope.mobileerror="";	
		}
		if(($scope.address==undefined)||($scope.address==""))
		{
			$scope.addresserror="pls enter address";
		}
		else
		{
		   $scope.addresserror="";	
		}
	}
});

obj.controller("contactus",function($scope){
	$scope.heading="CONTACT THE GROUP";
});
