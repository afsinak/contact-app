var app = angular.module('myApp', ['ngRoute','storageService'])// Create the AngularJS module myApp and Register the storageService with it

//ng-route config
.config(function ($routeProvider, $locationProvider){
  $routeProvider
    .when('/home', {
      templateUrl: 'default.html',
    })
    .when('/contact-info/:contact_index', {
      templateUrl: 'contact_info.html',
      controller: 'contactInfoCtrl'
    })
    .when('/add', {
      templateUrl: 'contact_form.html',
      controller: 'addContactCtrl'
    })
    .when('/edit/:contact_index', {
      templateUrl: 'contact_form.html',
      controller: 'editContactCtrl'
    })
    .otherwise({redirectTo: '/home'});
})

// controllers
/*.controller('navCtrl', function ($scope) {
  $scope.nav = {
    navItems: ['home', 'add'],
    selectedIndex: 0,
    navClick: function ($index) {
      $scope.nav.selectedIndex = $index;
    }
  };
})*/

.controller('homeCtrl',['$scope','getLocalStorage', function ($scope, getLocalStorage){
  $scope.contacts = getLocalStorage.getContacts();//Read the contact List from LocalStorage  
  // console.log("fromhomecontrol", $scope.contacts);
    /*added
    var index = $routeParams.contact_index;
    $scope.currentContact = $scope.contacts[index];
    //tillhere*/
  $scope.removeContact = function (item) {
    var index = $scope.contacts.indexOf(item);
    $scope.contacts.splice(index, 1);
    getLocalStorage.updateContacts($scope.contacts);
    $scope.removed = 'Contact successfully removed.';
  };




}])

.controller('contactInfoCtrl', ['$scope','$routeParams','getLocalStorage',function ($scope, $routeParams,getLocalStorage){
  
  // console.log($routeParams);
  var index = $routeParams.contact_index;
  $scope.currentContact = $scope.contacts[index];
  // console.log( $scope.currentContact);
}])

.controller('addContactCtrl', ['$scope','$location','getLocalStorage',function ($scope,$location,getLocalStorage) {
  //needed to show the correct button on the contact form
  $scope.path = $location.path();//was cpmmnted
  // console.log($scope.path);
  $scope.contacts=getLocalStorage.getContacts();
  // console.log("fromcontrol",$scope.contacts);

  $scope.addContact = function () {
    //console.log('kdfhods');
    // $scope.currentContact = $scope.contacts;
    console.log($scope.contacts);
    var contact = $scope.currentContact;
    
    // var objLength = Object.keys(cont);

    contact.id = $scope.contacts.length;
    console.log(contact.id);
    /*$scope.contacts.push(contact);*/
    $scope.contacts.push({'currentContact.name':$scope.currentContact.name,'currentContact.email':$scope.currentContact.email,'currentContact.phone':$scope.currentContact.phone,'currentContact.organisation':$scope.currentContact.organisation,'currentContact.notes':$scope.currentContact.notes});
            getLocalStorage.updateContacts($scope.contacts);
            console.log($scope.currentContact.name);
            $scope.currentContact.name='';
            $scope.currentContact.email='';
            $scope.currentContact.phone='';
            $scope.currentContact.organisation='';
            $scope.currentContact.notes='';
            // console.log('contact is ');
  };

}])

.controller('editContactCtrl', ['$scope','$routeParams','getLocalStorage',function ($scope, $routeParams,getLocalStorage){
  $scope.index = $routeParams.contact_index;
  $scope.currentContact = $scope.contacts[$scope.index];
  $scope.saveContact = function(){
    $scope.index = $routeParams.contact_index;
    $scope.currentContact = $scope.contacts[$scope.index];
    $scope.currentContact["currentContact.name"]=$scope.currentContact.name;
    $scope.currentContact["currentContact.email"]=$scope.currentContact.email;
    $scope.currentContact["currentContact.phone"]=$scope.currentContact.phone;
    $scope.currentContact["currentContact.organisation"]=$scope.currentContact.organisation;
    $scope.currentContact["currentContact.notes"]=$scope.currentContact.notes;
   
  };

  

 

}])

// directives
.directive('contact', function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'contact.html'
  }
})

/*services
.factory('ContactService', [function () {
  var factory = {};

  factory.getContacts = function () {
    return contactList;
  }

  // contact list, usually would be a separate database
  var contactList = [
    {id: 0, name: 'Ajay Takoor', email: 'ajay@gmail.com', phone: '7890765432', organisation: 'HCL', notes:'Friend.'},
    {id: 1, name: 'Zehan Falak', email: 'zehan@gmail.com', phone: '9023145678', organisation: 'Student', notes: 'Pending Money.'},
    {id: 2, name: 'George Manual', email: 'george@yahoo.com', phone:'9876544321', organisation: 'Business', notes: 'Loyal brother'},
    {id: 3, name: 'Surya Ram', email: 'surya@google.com', phone: '7890654321', organisation: 'IIT pro', notes: 'Knows nothing.'},
    {id: 4, name: 'Arya Kiran', email: 'arya@yahoo.com', phone: '980764512', organisation: 'Google', notes: 'Will meet next week.'},
    {id: 5, name: 'Joy Mathew', email: 'joy@gmail.com', phone: '8907654321', organisation: 'Foreign', notes: 'Uncle.'},
    {id: 6, name: 'Tesa GH', email: 'tesa12@gmail.com', phone: '9078653421', organisation: 'Samsung', notes: 'Lost friend.'},
    {id: 7, name: 'Sree Krishna', email: 'sreekrish45@gmail.com', phone: '9908765432', organisation: 'Zebronic', notes: 'Nobody expects.'},
    {id: 8, name: 'Helen Mathew', email: 'hmathew@gmail.com', phone: '8907654321', organisation: 'Dell', notes: 'Krishna fan'},
    {id: 9, name: 'Meherzan K', email: 'meherzan12@gmail.com', phone: '9087123456', organisation: 'Student', notes: 'Sweet bro.'},
    {id: 10, name: 'Paul George', email: 'paul@gmail.com', phone: '9908765432', organisation: 'Business', notes: 'Dont contact.'},
    {id: 11, name: 'Peter Leu', email: 'peter@gmail.com', phone: '7869054321', organisation: 'Saleforce', notes: 'Luvly.'},
  ];
  
  return factory;
}]);*/

//create angularjs module named storageservice
var storageService=angular.module('storageService',[]);
storageService.factory('getLocalStorage',function(){//Create getLocalStorage service to access getcontacts and updatecontacts method
  var contactList={};
  return{
    list:contactList,
    updateContacts:function(ContactsArr){
      if(window.localStorage && ContactsArr){//ls to add data
        localStorage.setItem("contacts",angular.toJson(ContactsArr));
      }
      ContactList=ContactsArr;
    },
    getContacts:function(){//get data from ls
      contactList=angular.fromJson(localStorage.getItem("contacts"));
      // console.log(contactList);
      return contactList ? contactList :[];
    }
  };
});

