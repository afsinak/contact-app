var obj=angular.module("myProject",["ngRoute"]);
obj.config(function($routeProvider){
	$routeProvider
	   .when("/joblist",{
		templateUrl : "joblist.html"
	})
	 .when("/signup",{
		templateUrl : "signup.html"
	})
	 .when("/login",{
		templateUrl : "login.html"
	})
	
	
});

obj.config(function($routeProvider){
	$routeProvider
	   .when("/dashboard",{
		templateUrl : "dashboard.html"
	})
	 .when("/browse",{
		templateUrl : "browse.html"
	})
	 .when("/applied",{
		templateUrl : "applied.html"
	})
	.when("/shortlisted",{
		templateUrl : "short.html"
	})
	
});

obj.config(function($routeProvider){
	$routeProvider
	   .when("/overview",{
		templateUrl : "overview.html"
	})
	 .when("/summary",{
		templateUrl : "summary.html"
	})
	 .when("/pinfo",{
		templateUrl : "pinfo.html"
	})
	.when("/educational",{
		templateUrl : "educational.html"
	})
	.when("/projects",{
		templateUrl : "projects.html"
	})
	.when("/designation",{
		templateUrl : "designation.html"
	})
});

obj.config(function($routeProvider){
	$routeProvider
	   .when("/chgpass",{
		templateUrl : "chgpass.html"
	})
	 .when("/actset",{
		templateUrl : "actset.html"
	})
	 .when("/payment",{
		templateUrl : "payment.html"
	})
	
	
});

obj.config(function($routeProvider){
	$routeProvider
	   .when("/",{
		templateUrl : "educational.html"
	})
});

//validation for profile summary
obj.controller("pageSummary",function($scope){
	$scope.pagesummary=function(){
		if(($scope.sum==undefined)||($scope.sum==""))
		{
			$scope.summaryerror="invalid summary !please try again";
		}
		else{
			$scope.summaryerror="";
		}
	}
});
obj.controller("pageOver",function($scope)
{
	$scope.pagetest=function()
	{
		if(($scope.fname==undefined)||($scope.fname==""))
		{
			$scope.nameerror="invalid name !please try again";
		}
		else{
			$scope.nameerror="";
		}
		if(($scope.mobile==undefined)||($scope.mobile=="")||(mobile.length>10)||(mobile.length<10))
		{
			$scope.mobileerror="invalid number !please try again";
		}
		else{
			$scope.mobileerror="";
		}
		if(($scope.email==undefined)||($scope.email==""))
		{
			$scope.emailerror="invalid email !please try again";
		}
		else{
			$scope.emailerror="";
		}
		if(($scope.pass==undefined)||($scope.pass==""))
		{
			$scope.passerror="invalid password !please try again";
		}
		else{
			$scope.passerror="";
		}
		
		if(($scope.city==undefined)||($scope.city==""))
		{
			$scope.cityerror="please select city";
		}
		else{
			$scope.cityerror="";
		}
		if(($scope.address==undefined)||($scope.address==""))
		{
			$scope.addresserror="please enter address";
		}
		else{
			$scope.addresseerror="";
		}
	}
});


//validation for signup controller start
obj.controller("pageOverview",function($scope)
{
    $scope.allcity=['ahmedabad','manglore','banglore','hyderabad'];
	$scope.agree=false;
	$scope.allday=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21',
		   '22','23','24','25','26','27','28','29','30','31']; 
    $scope. allmonth=['1','2','3','4','5','6','7','8','9','10','11','12'];
	$scope. allyear=['1990','1991','1992','1993','1994','1995','1996','1997','1998','1999','2000'];
	$scope.allgender=['male','female'];
	$scope. allstatus=['married','unmarried'];
    $scope.allrole=['Developer','Designer'];
	$scope.allteam=['4','6','8'];
	$scope.pagetest=function()
	{
		if(($scope.fname==undefined)||($scope.fname==""))
		{
			$scope.nameerror="invalid name !please try again";
		}
		else{
			$scope.nameerror="";
		}
		if(($scope.email==undefined)||($scope.email==""))
		{
			$scope.emailerror="invalid email !please try again";
		}
		else{
			$scope.emailerror="";
		}
		if(($scope.mobile==undefined)||($scope.mobile=="")||(mobile.length>10)||(mobile.length<10))
		{
			$scope.mobileerror="invalid number !please try again";
		}
		else{
			$scope.mobileerror="";
		}
		if(($scope.day==undefined)||($scope.day==""))
		{
			$scope.dayerror="invalid day !please try again";
		}
		else{
			$scope.dayerror="";
		}
		if(($scope.month==undefined)||($scope.month==""))
		{
			$scope.montherror="invalid month !please try again";
		}
		else{
			$scope.montherror="";
		}
		if(($scope.year==undefined)||($scope.year==""))
		{
			$scope.yearerror="invalid year !please try again";
		}
		else{
			$scope.yearerror="";
		}
		if(($scope.gender==undefined)||($scope.gender==""))
		{
			$scope.gendererror=" please select gender";
		}
		else{
			$scope.gendererror="";
		}
		if(($scope.statuss==undefined)||($scope.statuss==""))
		{
			$scope.statuserror=" please select status";
		}
		else{
			$scope.statuserror="";
		}
		if(($scope.pin==undefined)||($scope.pin==""))
		{
			$scope.pinerror=" please enter pin";
		}
		else{
			$scope.pinerror="";
		}
		if(($scope.resume==undefined)||($scope.resume==""))
		{
			$scope.resumeerror=" please enter heading";
		}
		else{
			$scope.resumeerror="";
		}
		if(($scope.pass==undefined)||($scope.pass==""))
		{
			$scope.passerror="invalid password !please try again";
		}
		else{
			$scope.passerror="";
		}
		
		if(($scope.city==undefined)||($scope.city==""))
		{
			$scope.cityerror="please select city";
		}
		else{
			$scope.cityerror="";
		}
		if(($scope.address==undefined)||($scope.address==""))
		{
			$scope.addresserror="please enter address";
		}
		else{
			$scope.addresseerror="";
		}
	}
});

//validation for educational

obj.controller("eduValidation",function($scope)
{
    $scope.degree=['BCom','BBM','BSC','BCC'];
	$scope. allyear=['1990','1991','1992','1993','1994','1995','1996','1997','1998','1999','2000'];
	$scope.grade=['A','B','C','D'];
});
